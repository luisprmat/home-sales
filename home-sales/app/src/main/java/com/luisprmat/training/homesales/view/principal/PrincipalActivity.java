package com.luisprmat.training.homesales.view.principal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.luisprmat.training.homesales.R;
import com.luisprmat.training.homesales.view.sales.SalesActivity;

import java.util.List;
import java.util.Map;

public class PrincipalActivity extends AppCompatActivity implements PrincipalMVP.View {

    private PrincipalMVP.Presenter presenter;

    RecyclerView rvCharges;
    FloatingActionButton btnSales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.loadCharges();
    }

    @Override
    public void onBackPressed() {
        presenter.logout();
    }

    private void initUI() {
        presenter = new PrincipalPresenter(this);

        rvCharges = findViewById(R.id.rv_charges);
        rvCharges.setLayoutManager(new LinearLayoutManager(this));

        btnSales = findViewById(R.id.btn_sales);
        btnSales.setOnClickListener(v -> presenter.newSale());
    }

    @Override
    public void loadCharges(List<ChargeInfo> charges) {
        ChargesAdapter adapter = new ChargesAdapter(charges);
        adapter.setOnItemClickListener(info -> {
            presenter.openPay(info);
//            Snackbar.make(rvCharges, "Cobrando a " + info.getClient(), Snackbar.LENGTH_SHORT)
//                    .show();
        });
        rvCharges.setAdapter(adapter);
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type, Map<String, String> params) {
        Intent intent = new Intent(this, type);
        if (params != null) {
            for (String key: params.keySet()) {
                intent.putExtra(key, params.get(key));
            }
        }
        startActivity(intent);
    }

    @Override
    public void confirmLogout() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("¿Está seguro de cerrar la sesión?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> presenter.logoutConfirmed())
                .setNegativeButton(android.R.string.no, null)
                .show();
    }
}