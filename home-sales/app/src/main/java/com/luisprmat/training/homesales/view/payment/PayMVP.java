package com.luisprmat.training.homesales.view.payment;

import androidx.appcompat.app.AppCompatActivity;

import com.luisprmat.training.homesales.view.principal.PrincipalActivity;

public interface PayMVP {
    interface Model {

        void saveAllPayment(String id);

        void saveAllPart(String id, Double value);

        void saveNoPayment(String id, String option);
    }

    interface Presenter {
        void savePayment();
    }

    interface View {
        String getSelectedOption();

        String getId();

        void showActivity(Class<? extends AppCompatActivity> type);

        String getPartValue();
    }
}
