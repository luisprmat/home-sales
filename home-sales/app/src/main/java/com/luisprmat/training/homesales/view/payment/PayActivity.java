package com.luisprmat.training.homesales.view.payment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.luisprmat.training.homesales.R;
import com.luisprmat.training.homesales.view.principal.PrincipalActivity;

public class PayActivity extends AppCompatActivity implements PayMVP.View {
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_CLIENT = "name";
    public static final String EXTRA_ADDRESS = "address";
    public static final String EXTRA_PART = "part";

    private PayMVP.Presenter presenter;

    private String id;

    TextView tvClient, tvAddress, tvPart;

    AppCompatButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        initUI();
    }

    private void initUI() {
        presenter = new PayPresenter(this);

        tvClient = findViewById(R.id.tv_client);
        tvAddress = findViewById(R.id.tv_address);
        tvPart = findViewById(R.id.tv_part);

        id = getIntent().getStringExtra(EXTRA_ID);
        tvClient.setText(getIntent().getStringExtra(EXTRA_CLIENT));
        tvAddress.setText(getIntent().getStringExtra(EXTRA_ADDRESS));
        tvPart.setText(getIntent().getStringExtra(EXTRA_PART));

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> {
            presenter.savePayment();
        });
    }

    @Override
    public String getSelectedOption() {
        return "all";
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public String getPartValue() {
        return null;
    }
}