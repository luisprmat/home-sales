package com.luisprmat.training.homesales.view.sales;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.snackbar.Snackbar.Callback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.luisprmat.training.homesales.R;

public class SalesActivity extends AppCompatActivity implements SalesMVP.View {
    private SalesMVP.Presenter presenter;

    TextInputLayout tilClient;
    TextInputEditText etClient;
    TextInputLayout tilAddress;
    TextInputEditText etAddress;
    TextInputLayout tilAmount;
    TextInputEditText etAmount;
    TextInputLayout tilParts;
    TextInputEditText etParts;
    TextInputLayout tilPeriodicity;
    AppCompatAutoCompleteTextView spPeriodicity;
    TextInputEditText etPart;
    MaterialButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);

        initUI(savedInstanceState);
    }

    private void initUI(Bundle savedInstanceState) {
        presenter = new SalesPresenter(this);

        tilClient = findViewById(R.id.til_client);
        etClient = findViewById(R.id.et_client);

        tilAddress = findViewById(R.id.til_address);
        etAddress = findViewById(R.id.et_address);

        tilAmount = findViewById(R.id.til_amount);
        etAmount = findViewById(R.id.et_amount);

        tilParts = findViewById(R.id.til_parts);
        etParts = findViewById(R.id.et_parts);

        tilPeriodicity = findViewById(R.id.til_periodicity);
        spPeriodicity = findViewById(R.id.sp_periodicity);

        String[] periodicities = getResources().getStringArray(R.array.periodicity);
        spPeriodicity.setAdapter(new ArrayAdapter<String>(SalesActivity.this, R.layout.item_periodicity, periodicities));

        etPart = findViewById(R.id.et_part);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> {
            presenter.calculatePart();
            presenter.saveSale();
        });

        //TODO Cargar info después de rotación

        /*
        if (savedInstanceState != null) {
            etAmount.setText(savedInstanceState.getString("amount", ""));
        }
        */
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        //TODO Guardar la información de la rotación

        //outPersistentState.putString("amount", etAmount.getText().toString());
    }

    @Override
    public SaleInfo getSaleInfo() {
        return new SaleInfo(
                etClient.getText().toString().trim(),
                etAddress.getText().toString().trim(),
                etAmount.getText().toString().trim(),
                etParts.getText().toString().trim(),
                spPeriodicity.getText().toString().trim(),
                etPart.getText().toString().trim()
        );
    }

    @Override
    public void showErrorClient(String message) {
        tilClient.setError(message);
    }

    @Override
    public void showErrorAddress(String message) {
        tilAddress.setError(message);
    }

    @Override
    public void showErrorAmount(String message) {
        tilAmount.setError(message);
    }

    @Override
    public void showErrorPartsNumber(String message) {
        tilParts.setError(message);
    }

    @Override
    public void showErrorPeriodicity(String message) {
        tilPeriodicity.setError(message);
    }

    @Override
    public void showPartValue(String value) {
        etPart.setText(value);
    }

    @Override
    public void showSuccessMessage(String message, View.OnClickListener listener) {
        Snackbar.make(etAmount, message, Snackbar.LENGTH_SHORT)
                .setCallback(new Callback() {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, @DismissEvent int event) {
                        if (event == Callback.DISMISS_EVENT_TIMEOUT) {
                            listener.onClick(etAmount);
                        }
                    }
                })
                .show();
    }


    @Override
    public void closeActivity() {
        finish();
    }
}