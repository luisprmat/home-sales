package com.luisprmat.training.homesales.view.principal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.luisprmat.training.homesales.R;

import java.util.List;

public class ChargesAdapter extends RecyclerView.Adapter<ChargesAdapter.ViewHolder> {

    List<ChargeInfo> data;
    private OnItemClickListener onItemClickListener;

    public ChargesAdapter(List<ChargeInfo> data) {
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_charges, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChargesAdapter.ViewHolder holder, int position) {
        holder.getTvClient().setText(data.get(position).getClient());
        holder.getTvAddress().setText(data.get(position).getAddress());
        holder.getTvPart().setText(data.get(position).getPart());
        holder.setInfo(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private MaterialTextView tvClient;
        private MaterialTextView tvAddress;
        private MaterialTextView tvPart;

        private ChargeInfo info;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvClient = itemView.findViewById(R.id.tv_client);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvPart = itemView.findViewById(R.id.tv_part);

            if (onItemClickListener != null) {
                itemView.setOnClickListener(this);
            }
        }

        public void setInfo(ChargeInfo info) {
            this.info = info;

            tvClient.setText(info.getClient());
            tvAddress.setText(info.getAddress());
            tvPart.setText(info.getPart());
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(info);
            }
        }

        public MaterialTextView getTvClient() {
            return tvClient;
        }

        public MaterialTextView getTvAddress() {
            return tvAddress;
        }

        public MaterialTextView getTvPart() {
            return tvPart;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ChargeInfo info);
    }
}
