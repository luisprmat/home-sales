package com.luisprmat.training.homesales.view.principal;

public class ChargeInfo {
    private String id;
    private String client;
    private String part;
    private String address;

    public ChargeInfo(String client, String address, String part) {
        this(null, client, address, part);
    }

    public ChargeInfo(String id, String client, String address, String part) {
        this.id = id;
        this.client = client;
        this.address = address;
        this.part = part;
    }

    public String getId() {
        return id;
    }

    public String getClient() {
        return client;
    }

    public String getAddress() {
        return address;
    }

    public String getPart() {
        return part;
    }

}
