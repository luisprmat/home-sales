package com.luisprmat.training.homesales.view.sales;

public class SaleInfo {
    private String client;
    private String address;
    private String amount;
    private String partsNumber;
    private String periodicity;
    private String fee;

    public SaleInfo(String client,String address ,String amount, String partsNumber, String periodicity, String fee) {
        this.client = client;
        this.address = address;
        this.amount = amount;
        this.partsNumber = partsNumber;
        this.periodicity = periodicity;
        this.fee = fee;
    }

    public String getClient() {
        return client;
    }

    public String getAddress() {
        return address;
    }

    public String getAmount() {
        return amount;
    }

    public String getPartsNumber() {
        return partsNumber;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public String getFee() {
        return fee;
    }
}
