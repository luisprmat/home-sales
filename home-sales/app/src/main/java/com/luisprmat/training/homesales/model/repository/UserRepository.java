package com.luisprmat.training.homesales.model.repository;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.luisprmat.training.homesales.model.entity.User;
import com.luisprmat.training.homesales.view.login.MainMVP;

import java.util.List;

public class UserRepository implements MainMVP.Model {
    private static final String TAG = UserRepository.class.getSimpleName();

    private MainMVP.Presenter mainPresenter;
    private FirebaseAuth mAuth;

    private List<User> users;

    public UserRepository() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void setMainPresenter(MainMVP.Presenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }

    @Override
    public void validateUsernamePassword(String username, String password) {
        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mainPresenter.authenticationSuccessful();
                        } else {
                            mainPresenter.authenticationFailure(task.getException().getMessage());
                        }
                    }
                });
    }

    @Override
    public boolean isAuthenticated() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        return currentUser != null;
    }
}
