package com.luisprmat.training.homesales.view.principal;

import com.luisprmat.training.homesales.model.repository.ChargesRepository;
import com.luisprmat.training.homesales.view.login.MainActivity;
import com.luisprmat.training.homesales.view.payment.PayActivity;
import com.luisprmat.training.homesales.view.sales.SalesActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrincipalPresenter implements PrincipalMVP.Presenter {
    private final PrincipalMVP.View view;
    private final PrincipalMVP.Model model;

    public PrincipalPresenter(PrincipalMVP.View view) {
        this.view = view;
        this.model = new ChargesRepository(view.getApplicationContext(), this);
    }

    @Override
    public void loadCharges() {
        model.loadCharges();
    }

    @Override
    public void loadCharges(List<ChargeInfo> charges) {
        view.loadCharges(charges);
    }

    @Override
    public void newSale() {
        view.showActivity(SalesActivity.class, null);
    }

    @Override
    public void logout() {
        view.confirmLogout();
    }

    @Override
    public void logoutConfirmed() {
        model.logout();
        view.showActivity(MainActivity.class, null);
    }

    @Override
    public void openPay(ChargeInfo info) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(PayActivity.EXTRA_ID, info.getId());
        parameters.put(PayActivity.EXTRA_CLIENT, info.getClient());
        parameters.put(PayActivity.EXTRA_ADDRESS, info.getAddress());
        parameters.put(PayActivity.EXTRA_PART, info.getPart());

        view.showActivity(PayActivity.class, parameters);
    }
}
