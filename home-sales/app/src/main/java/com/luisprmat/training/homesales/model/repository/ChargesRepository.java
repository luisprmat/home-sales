package com.luisprmat.training.homesales.model.repository;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.luisprmat.training.homesales.model.entity.Sale;
import com.luisprmat.training.homesales.view.payment.PayMVP;
import com.luisprmat.training.homesales.view.principal.ChargeInfo;
import com.luisprmat.training.homesales.view.principal.PrincipalMVP;
import com.luisprmat.training.homesales.view.principal.PrincipalPresenter;
import com.luisprmat.training.homesales.view.sales.SaleInfo;
import com.luisprmat.training.homesales.view.sales.SalesMVP;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChargesRepository implements PrincipalMVP.Model, SalesMVP.Model, PayMVP.Model {
    private static final String TAG = "ChargesRepository";

    private PrincipalPresenter principalPresenter;

    private FirebaseDatabase database = null;
    private DatabaseReference myRef = null;

    public Integer idCounter;

    private SimpleDateFormat format;

    public ChargesRepository(Context context, PrincipalPresenter principalPresenter) {
        this.principalPresenter = principalPresenter;

        this.format = new SimpleDateFormat("yyyy-MM-dd");

        myRef = FirebaseDatabase.getInstance().getReference();
        getIdCounter();
    }

    private void getIdCounter() {
        myRef.child("saleCounter").get().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.e(TAG, "Error recibiendo datos", task.getException());
            } else {
                idCounter = task.getResult().getValue(Integer.class);
            }
        });
    }

    private void saveIdCounter() {
        myRef.child("saleCounter").setValue(idCounter);
    }

    @Override
    public void loadCharges() {
        /*
        List<Sale> sales = saleDao.getAllSales();
        for (Sale sale : sales) {
            response.add(new ChargeInfo(sale.getClient(), sale.getAddress(),
                    sale.getPart().toString()));
        }
        */

        myRef.child("sales").get().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.e(TAG, "Error recibiendo datos", task.getException());
            } else {
                List<ChargeInfo> response = new ArrayList<>();

                Log.d("TAG", String.valueOf(task.getResult().getValue()));
                Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                for (DataSnapshot snapshot : task.getResult().getChildren()) {
                    Log.d("TAG", "Elementos: " + snapshot);
                    Sale sale = snapshot.getValue(Sale.class);
                    Log.d("TAG", "Sale: " + sale);
                    response.add(new ChargeInfo(sale.getUid().toString(), sale.getClient(), sale.getAddress(),
                            sale.getPart().toString()));
                }

                principalPresenter.loadCharges(response);

            }
        });
    }

    @Override
    public void logout() {
        FirebaseAuth.getInstance().signOut();
    }

    public void saveNewSale(SaleInfo info) {
        Sale sale = new Sale();
//        TODO Assign id
        sale.setUid(idCounter++);
        sale.setClient(info.getClient());
        sale.setAddress(info.getAddress());
        sale.setAmount(Double.valueOf(info.getAmount()));
        sale.setParts(Integer.valueOf(info.getPartsNumber()));
        sale.setPeriodicity(info.getPeriodicity());
        sale.setPart(Double.valueOf(info.getFee()));

//        saleDao.insert(sale);
        myRef.child("sales").child(sale.getUid().toString()).setValue(sale);
        saveIdCounter();
    }

    @Override
    public void saveAllPayment(String id) {
        myRef.child("sales").child(id).child("payments")
                .child(format.format(new Date()))
                .setValue("Pay All");
    }

    @Override
    public void saveAllPart(String id, Double value) {
        myRef.child("sales").child(id).child("payments")
                .child(format.format(new Date()))
                .setValue(value);
    }

    @Override
    public void saveNoPayment(String id, String option) {
        myRef.child("sales").child(id).child("nopayment")
                .child(format.format(new Date()))
                .setValue(option);
    }
}
