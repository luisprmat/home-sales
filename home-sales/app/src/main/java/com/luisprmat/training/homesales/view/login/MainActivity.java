package com.luisprmat.training.homesales.view.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.luisprmat.training.homesales.R;

public class MainActivity extends AppCompatActivity implements MainMVP.View {

    private MainMVP.Presenter presenter;

    TextInputLayout tilUsername;
    TextInputLayout tilPassword;
    TextInputEditText etUsername;
    TextInputEditText etPassword;

    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitUI();
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.authenticate();
    }

    private void InitUI() {
        presenter = new MainPresenter(this);

        tilUsername = findViewById(R.id.til_username);
        etUsername = findViewById(R.id.et_username);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> {
            tilUsername.setError("");
            tilPassword.setError("");

            presenter.login();
        });
    }

    @Override
    public LoginInfo getLoginInfo() {
        return new LoginInfo(
                etUsername.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String message) {
        tilUsername.setError(message);
    }

    @Override
    public void showPasswordError(String message) {
        tilPassword.setError(message);
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(btnLogin, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }
}