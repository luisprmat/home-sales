package com.luisprmat.training.homesales.view.principal;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.luisprmat.training.homesales.view.sales.SalesActivity;

import java.util.List;
import java.util.Map;

public interface PrincipalMVP {
    interface Model {
        void loadCharges();

        void logout();
    }

    interface Presenter {
        void loadCharges();

        void loadCharges(List<ChargeInfo> charges);

        void newSale();

        void logout();

        void logoutConfirmed();

        void openPay(ChargeInfo info);
    }

    interface View {
        Context getApplicationContext();

        void loadCharges(List<ChargeInfo> charges);

        void showActivity(Class<? extends AppCompatActivity> type, Map<String, String> params);

        void confirmLogout();
    }

}
