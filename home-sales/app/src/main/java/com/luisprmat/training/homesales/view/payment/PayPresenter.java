package com.luisprmat.training.homesales.view.payment;

import com.luisprmat.training.homesales.model.repository.ChargesRepository;
import com.luisprmat.training.homesales.view.principal.PrincipalActivity;

public class PayPresenter implements PayMVP.Presenter {
    private PayMVP.Model model;
    private PayMVP.View view;

    public PayPresenter(PayMVP.View view) {
        this.view = view;
        this.model = new ChargesRepository(null, null);
    }

    @Override
    public void savePayment() {
        String idCharge = view.getId();
        String option = view.getSelectedOption();

        if (option.equals("all")) {
            model.saveAllPayment(idCharge);
        } else if (option.equals("part")) {
            String value = view.getPartValue();
            model.saveAllPart(idCharge, Double.valueOf(value));
        } else {
            model.saveNoPayment(idCharge, option);
        }

        view.showActivity(PrincipalActivity.class);
    }
}
