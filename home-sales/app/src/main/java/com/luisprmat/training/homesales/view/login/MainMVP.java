package com.luisprmat.training.homesales.view.login;

import androidx.appcompat.app.AppCompatActivity;

public interface MainMVP {
    interface Model {
        void validateUsernamePassword(String username, String password);

        void setMainPresenter(Presenter presenter);

        boolean isAuthenticated();
    }

    interface Presenter {
        void login();

        void authenticate();

        void authenticationSuccessful();

        void authenticationFailure(String message);
    }

    interface View {
        LoginInfo getLoginInfo();

        void showEmailError(String message);

        void showPasswordError(String message);

        void showActivity(Class<? extends AppCompatActivity> type);

        void showErrorMessage(String message);
    }
}
