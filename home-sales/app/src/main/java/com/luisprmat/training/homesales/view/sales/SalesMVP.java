package com.luisprmat.training.homesales.view.sales;

import android.content.Context;

public interface SalesMVP {
    interface Model {
        void saveNewSale(SaleInfo info);
    }

    interface Presenter {
        void calculatePart();
        void saveSale();
    }

    interface View {
        SaleInfo getSaleInfo();

        void showErrorClient(String message);
        void showErrorAddress(String message);
        void showErrorAmount(String message);
        void showErrorPartsNumber(String message);
        void showErrorPeriodicity(String message);

        void showPartValue(String value);
        void showSuccessMessage(String message, android.view.View.OnClickListener listener);
        void closeActivity();

        Context getApplicationContext();
    }
}
