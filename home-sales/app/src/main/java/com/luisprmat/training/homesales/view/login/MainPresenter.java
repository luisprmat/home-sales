package com.luisprmat.training.homesales.view.login;

import com.luisprmat.training.homesales.model.repository.UserRepository;
import com.luisprmat.training.homesales.view.principal.PrincipalActivity;

public class MainPresenter implements MainMVP.Presenter {
    private final MainMVP.View view;
    private final MainMVP.Model model;

    public MainPresenter(MainMVP.View view) {
        this.view = view;
        this.model = new UserRepository();
        this.model.setMainPresenter(this);
    }

    @Override
    public void login() {
        LoginInfo info = view.getLoginInfo();

        //Validar datos
        if (info.getEmail() == null
                || info.getEmail().trim().isEmpty()
                || !info.getEmail().contains("@")) {
            view.showEmailError("Correo electrónico no válido");
            return;
        }

        if (info.getPassword() == null
                || info.getPassword().trim().isEmpty()
                || info.getPassword().trim().length() < 6) {
            view.showPasswordError("Contraseña no válida");
            return;
        }

        model.validateUsernamePassword(info.getEmail(), info.getPassword());
    }

    @Override
    public void authenticate() {
        if (model.isAuthenticated()) {
            view.showActivity(PrincipalActivity.class);
        }
    }

    @Override
    public void authenticationSuccessful() {
        view.showActivity(PrincipalActivity.class);
    }

    @Override
    public void authenticationFailure(String message) {
        view.showPasswordError(message);
    }

}
