package com.luisprmat.training.homesales.view.sales;

import com.luisprmat.training.homesales.model.repository.ChargesRepository;

public class SalesPresenter implements SalesMVP.Presenter {
    private final SalesMVP.View view;
    private final SalesMVP.Model model;

    public SalesPresenter(SalesMVP.View view) {
        this.view = view;
        this.model = new ChargesRepository(view.getApplicationContext(), null);
    }

    @Override
    public void calculatePart() {
        SaleInfo info = view.getSaleInfo();

        // Validate required fields
        if (info.getAmount() == null ||
                info.getAmount().isEmpty()) {
            view.showErrorClient("El monto es un campo obligatorio");
            return;
        }

        Double amount = null;
        try {
            amount = Double.valueOf(info.getAmount());
        } catch (Exception e) {
            view.showErrorAmount("El monto no tiene formato numérico");
            return;
        }

        if (info.getPartsNumber() == null ||
                info.getPartsNumber().isEmpty()) {
            view.showErrorClient("El número de cuotas es un campo obligatorio");
            return;
        }

        Integer partsNumber = null;
        try {
            partsNumber = Integer.valueOf(info.getPartsNumber());
        } catch (Exception e) {
            view.showErrorPartsNumber("El núemero de cuotas no tiene formato numérico");
            return;
        }

        if (info.getPeriodicity() == null || info.getPeriodicity().isEmpty()) {
            view.showErrorPeriodicity("La periodicidad es un campo obligatorio");
            return;
        }

        //Calculate fee
        Double interest = 0d;

        switch (info.getPeriodicity()) {
            case "Diario":
                interest = 1d;
                break;

            case "Semanal":
                interest = 3d;
                break;

            case "Quincenal":
                interest = 5d;
                break;

            case "Mensual":
                interest = 10d;
                break;

            case "Trimestral":
                interest = 20d;
                break;

            case "Semestral":
                interest = 30d;
                break;

            case "Anual":
                interest = 50d;
                break;

            default:
                view.showErrorPeriodicity("Periodicidad no tiene un valor válido");
                return;
        }

        Double fee = amount * (100 + interest) / 100;
        fee = (double) Math.round(fee / partsNumber);

        view.showPartValue(fee.toString());
    }

    @Override
    public void saveSale() {
        SaleInfo info = view.getSaleInfo();

        // Validar campos
        if (info.getClient() == null
                || info.getClient().isEmpty()) {
            view.showErrorClient("El cliente es un campo obligatorio");
            return;
        }

        if (info.getAddress() == null ||
                info.getAddress().isEmpty()) {
            view.showErrorClient("La dirección es un campo obligatorio");
            return;
        }

        if (info.getAmount() == null ||
                info.getAmount().isEmpty()) {
            view.showErrorClient("El monto es un campo obligatorio");
            return;
        }

        try {
            Double.valueOf(info.getAmount());
        } catch (Exception e) {
            view.showErrorAmount("El monto debe ser numérico");
            return;
        }

        if (info.getPartsNumber() == null ||
                info.getPartsNumber().isEmpty()) {
            view.showErrorClient("El número de cuotas es un campo obligatorio");
            return;
        }

        try {
            Integer.valueOf(info.getPartsNumber());
        } catch (Exception e) {
            view.showErrorPartsNumber("El núemero de cuotas no tiene formato numérico");
            return;
        }

        model.saveNewSale(info);

        view.showSuccessMessage("Venta almacenada exitosamente", v -> view.closeActivity());
    }
}
